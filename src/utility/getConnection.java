/*
 * getConnection.java
 *
 * Created on 05 April 2009, 9:00
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;


/**
 *
 * @author Laptop Rara
 */
public class getConnection {
    Connection koneksiLocal;
    public String jdbc = "";
    public String url = "";
    public String user = "";
    public String pass = "";
    /** Creates a new instance of getConnection */
    public getConnection() {
        
        String folder = System.getProperty("user.dir");
        String driverJDBC = "jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=" + folder + "\\" + "Setting" + "\\" + "Setting.mdb;PWD=javakuternate24";
        koneksiLocal = getConnection("sun.jdbc.odbc.JdbcOdbcDriver", driverJDBC, "", "javakuternate24");
        if(koneksiLocal != null){
            String sql = "SELECT * FROM db";
            
            try {
                java.sql.PreparedStatement stat = koneksiLocal.prepareStatement(sql);
                java.sql.ResultSet rset = stat.executeQuery();
                if (rset.next()) {
                    jdbc = rset.getString("driver");
                    url = rset.getString("url");
                    user = rset.getString("user");
                    pass = rset.getString("pass");
                    
                    
                }
            } catch (SQLException ex) {
               
                System.out.println("ERROR DATABASE "+ ex.getMessage());
            }
        }
        
    }
  
    public Connection getConnection(String driverJDBC, String url, String user, String password){
        boolean driver = false;
        Connection koneksi = null;
        try {
            // Penyambungan Driver
            Class.forName(driverJDBC);
            driver = true;
            // Apabila Kelas Driver Tidak Ditemukan
        } catch (ClassNotFoundException not) {
            koneksi = null;
            System.out.println(not);
        }
        
        if (driver == true) {
            
            try {
                koneksi = java.sql.DriverManager.getConnection(url, user, password);
                
            } // Akhir try
            catch (java.sql.SQLException sqln) {
                koneksi = null;
                System.out.println(sqln);
                
            }
        }
        
        return koneksi;
    }
}
