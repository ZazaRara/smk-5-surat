
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.UnsupportedLookAndFeelException;

import utility.getConnection;
import utility.penangananKomponen;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author iweks
 */
public class jendelaUtama extends javax.swing.JFrame {

    getConnection u;
    Connection kon;
    penangananKomponen kom;
    pengaturan p_pengaturan;
    String user = "";
    String folder;
    suratMasuk suratMasuk;
    suratKeluar suratKeluar;
    laporanSurat laporanSurat;
    createUser createUser;

    /**
     * Creates new form jendelaUtama
     */
    public jendelaUtama() {
        u = new getConnection();
        kon = u.getConnection(u.jdbc, u.url, u.user, u.pass);
        kom = new penangananKomponen();
        if (kon == null) {
            setVisible(false);
            new pengaturan();
        } else {
            folder = System.getProperty("user.dir");

            initComponents();

            final DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy -- HH:mm:ss");
            ActionListener taskPerformer = new ActionListener() {
                public void actionPerformed(ActionEvent evt) {

                    java.util.Date date = new java.util.Date();

                    String datestring = dateFormat.format(date);
                    jam.setText(datestring);
                }
            };
            // Timer
            new Timer(1000, taskPerformer).start();

            //  toolbar.setVisible(false);
            java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
            setSize(screen);

            //  createMenu();
            setLocation(0, 0);
            setVisible(true);
            tampil();

        }
        //  initComponents();
    }

    private void createMenu() {

        String sql = "SELECT peran FROM smk5_user WHERE userid = '" + user + "'";
        String dt = kom.getStringSQL(kon, sql);

        boolean ok = false;
        if (dt.equalsIgnoreCase("Administrator")) {
            ok = true;
        }
        mUser.setVisible(ok);
    }

    private void tampil() {
        menu.setVisible(false);

        toolbar.setVisible(false);
        java.awt.Dimension screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int wt = 360;
        int ht = 170;
        int w = screen.width;
        int h = screen.height;
        login.setLocation((w - wt) / 2, ((h - ht) / 2) - (ht / 2));
        login.setSize(wt, ht);
        d_user.requestFocus();
        login.setVisible(true);
    }

    public void showFrame(JInternalFrame jss, String judul) {
        toolbar.setVisible(true);
        boolean frame = this.isLoaded(judul);
        Dimension d = tengah.getSize();
        if (!frame) {
            try {
                jss.setSize(d);
                this.tengah.removeAll();
                this.tengah.add(jss);
                jss.setMaximum(true);
                jss.show();
                jss.setSelected(true);

            } catch (java.beans.PropertyVetoException e) {
            }
        }
    }

    public boolean isLoaded(String FormTitle) {
        javax.swing.JInternalFrame Form[] = this.tengah.getAllFrames();
        for (int i = 0; i < Form.length; i++) {
            if (Form[i].getTitle().equalsIgnoreCase(FormTitle)) {
                Form[i].setLocation(0, 0);

                Form[i].show();

                try {
                    Form[i].setIcon(false);
                    Form[i].setSelected(true);
                } catch (java.beans.PropertyVetoException e) {
                }
                return true;
            }
        }
        return false;
    }

    private boolean login(String user, String pass) {
        boolean hasil = false;
        int a = 0;
        String sql = "SELECT count(userid) FROM smk5_user WHERE userid = '" + user + "' AND pass = MD5('" + pass + "')";

        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            while (rset.next()) {
                String aa = rset.getString(1);
                a = Integer.parseInt(aa);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (a > 0) {
            hasil = true;
        }

        return hasil;
    }

    private void goLogin() {
        String a = d_user.getText();
        String b = d_pass.getText();
        boolean ok = login(a, b);
        if (ok) {

            suksesLogin();
            login.dispose();

        } else {
            JOptionPane.showMessageDialog(login, "Maaf, User ID dan password tidak sesuai");
            d_user.setText("");
            d_pass.setText("");
            d_user.requestFocus();
        }
    }

    private void suksesLogin() {

        
        user = this.d_user.getText();
        label_user.setText("User Login : " + user);
//        usernameku.setText(user);

        //  tambah_panel_menu();
        menu.setVisible(true);
        toolbar.setVisible(true);
        createMenu();
        if (suratMasuk == null) {
            suratMasuk = new suratMasuk(user, kon);
        }
        showFrame(suratMasuk, "Surat Masuk");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        login = new javax.swing.JDialog();
        jPanel4 = new javax.swing.JPanel();
        d_user = new javax.swing.JTextField();
        d_pass = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        b_masuk = new javax.swing.JButton();
        b_keluar = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        toolbar = new javax.swing.JToolBar();
        jPanel6 = new javax.swing.JPanel();
        logo_utama = new javax.swing.JLabel();
        tengah = new javax.swing.JDesktopPane();
        jToolBar1 = new javax.swing.JToolBar();
        jPanel9 = new javax.swing.JPanel();
        versi = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        label_user = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jam = new javax.swing.JLabel();
        kiri = new javax.swing.JToolBar();
        menu = new javax.swing.JPanel();
        mKas = new javax.swing.JButton();
        mRugi = new javax.swing.JButton();
        mLaporan = new javax.swing.JButton();
        mUser = new javax.swing.JButton();
        mKeluar = new javax.swing.JButton();

        login.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        login.setTitle("LOGIN");
        login.setModal(true);
        login.setResizable(false);
        login.getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        d_user.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                d_userKeyReleased(evt);
            }
        });
        jPanel4.add(d_user, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, 150, -1));

        d_pass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                d_passKeyReleased(evt);
            }
        });
        jPanel4.add(d_pass, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 50, 150, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("User ID");
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 20, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Password");
        jPanel4.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, -1, 20));

        jPanel5.setBackground(new java.awt.Color(255, 51, 51));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        b_masuk.setText("Masuk");
        b_masuk.setPreferredSize(new java.awt.Dimension(83, 25));
        b_masuk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_masukActionPerformed(evt);
            }
        });
        jPanel5.add(b_masuk, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 10, -1, -1));

        b_keluar.setText("Keluar");
        b_keluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_keluarActionPerformed(evt);
            }
        });
        jPanel5.add(b_keluar, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 10, -1, -1));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/setting.png"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 30, -1));

        jPanel4.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 390, 50));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/logo_smk.png"))); // NOI18N
        jPanel4.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        login.getContentPane().add(jPanel4);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(this.MAXIMIZED_BOTH);
        setUndecorated(true);

        toolbar.setFloatable(false);
        toolbar.setRollover(true);

        jPanel6.setOpaque(false);
        jPanel6.setPreferredSize(new java.awt.Dimension(480, 90));
        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        logo_utama.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/logo.png"))); // NOI18N
        jPanel6.add(logo_utama);

        toolbar.add(jPanel6);

        getContentPane().add(toolbar, java.awt.BorderLayout.PAGE_START);

        tengah.setBackground(new java.awt.Color(153, 153, 255));

        javax.swing.GroupLayout tengahLayout = new javax.swing.GroupLayout(tengah);
        tengah.setLayout(tengahLayout);
        tengahLayout.setHorizontalGroup(
            tengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 884, Short.MAX_VALUE)
        );
        tengahLayout.setVerticalGroup(
            tengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 343, Short.MAX_VALUE)
        );

        getContentPane().add(tengah, java.awt.BorderLayout.CENTER);

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        jPanel9.setOpaque(false);
        jPanel9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 8, 8));

        versi.setText("versi 1.1");
        jPanel9.add(versi);

        jToolBar1.add(jPanel9);

        jPanel11.setOpaque(false);
        jPanel11.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 8, 8));

        label_user.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jPanel11.add(label_user);

        jToolBar1.add(jPanel11);

        jPanel10.setOpaque(false);
        jPanel10.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 8, 5));

        jam.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jPanel10.add(jam);

        jToolBar1.add(jPanel10);

        getContentPane().add(jToolBar1, java.awt.BorderLayout.PAGE_END);

        kiri.setFloatable(false);
        kiri.setRollover(true);

        menu.setOpaque(false);
        menu.setPreferredSize(new java.awt.Dimension(70, 341));

        mKas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/surat_masuk.png"))); // NOI18N
        mKas.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mKas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mKasActionPerformed(evt);
            }
        });
        menu.add(mKas);

        mRugi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/surat_keluar.png"))); // NOI18N
        mRugi.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mRugi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mRugiActionPerformed(evt);
            }
        });
        menu.add(mRugi);

        mLaporan.setBackground(new java.awt.Color(255, 255, 255));
        mLaporan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/laporan_surat.jpg"))); // NOI18N
        mLaporan.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mLaporan.setPreferredSize(new java.awt.Dimension(55, 55));
        mLaporan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mLaporanActionPerformed(evt);
            }
        });
        menu.add(mLaporan);

        mUser.setBackground(new java.awt.Color(255, 255, 255));
        mUser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/user-add-icon.png"))); // NOI18N
        mUser.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mUser.setPreferredSize(new java.awt.Dimension(55, 55));
        mUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mUserActionPerformed(evt);
            }
        });
        menu.add(mUser);

        mKeluar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/gambar/keluar.png"))); // NOI18N
        mKeluar.setMargin(new java.awt.Insets(0, 0, 0, 0));
        mKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mKeluarActionPerformed(evt);
            }
        });
        menu.add(mKeluar);

        kiri.add(menu);

        getContentPane().add(kiri, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void d_userKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_d_userKeyReleased
        if (evt.getKeyCode() == evt.VK_ENTER) {
            d_pass.requestFocus();
        }
    }//GEN-LAST:event_d_userKeyReleased

    private void d_passKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_d_passKeyReleased
        if (evt.getKeyCode() == evt.VK_ENTER) {
            goLogin();
        }
    }//GEN-LAST:event_d_passKeyReleased

    private void b_masukActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_masukActionPerformed
        goLogin();
    }//GEN-LAST:event_b_masukActionPerformed

    private void b_keluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_keluarActionPerformed
        System.exit(1);
    }//GEN-LAST:event_b_keluarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        login.dispose();
        setVisible(false);
        new pengaturan();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void mRugiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mRugiActionPerformed
        if (suratKeluar == null) {
            suratKeluar = new suratKeluar(user, kon);
        }
        showFrame(suratKeluar, "Surat Keluar");
    }//GEN-LAST:event_mRugiActionPerformed

    private void mKasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mKasActionPerformed
        if (suratMasuk == null) {
            suratMasuk = new suratMasuk(user, kon);
        }
        showFrame(suratMasuk, "Surat Masuk");
    }//GEN-LAST:event_mKasActionPerformed

    private void mLaporanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mLaporanActionPerformed
        if (laporanSurat == null) {
            laporanSurat = new laporanSurat(user, kon);
        }
        showFrame(laporanSurat, "Laporan");
    }//GEN-LAST:event_mLaporanActionPerformed

    private void mKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mKeluarActionPerformed
        this.tengah.removeAll();
        this.repaint();
        this.d_pass.setText("");
        this.d_user.setText("");
        toolbar.setVisible(false);
        label_user.setText("");
        tampil();
    }//GEN-LAST:event_mKeluarActionPerformed

    private void mUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mUserActionPerformed
        if (createUser == null) {
            createUser = new createUser(user, kon);
        }
        showFrame(createUser, "Laporan");
    }//GEN-LAST:event_mUserActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            javax.swing.UIManager.setLookAndFeel("org.fife.plaf.Office2003.Office2003LookAndFeel");
            // JFrame.setDefaultLookAndFeelDecorated(true);
            new jendelaUtama();

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton b_keluar;
    private javax.swing.JButton b_masuk;
    private javax.swing.JPasswordField d_pass;
    private javax.swing.JTextField d_user;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel jam;
    private javax.swing.JToolBar kiri;
    private javax.swing.JLabel label_user;
    private javax.swing.JDialog login;
    private javax.swing.JLabel logo_utama;
    private javax.swing.JButton mKas;
    private javax.swing.JButton mKeluar;
    private javax.swing.JButton mLaporan;
    private javax.swing.JButton mRugi;
    private javax.swing.JButton mUser;
    private javax.swing.JPanel menu;
    private javax.swing.JDesktopPane tengah;
    private javax.swing.JToolBar toolbar;
    private javax.swing.JLabel versi;
    // End of variables declaration//GEN-END:variables
}
